package edu.unc.cscc.timeline.model.user;

import java.time.Instant;
import java.util.Optional;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import edu.unc.cscc.timeline.model.TLEntity;

/**
 * Represents a user within timeline.  A "user" may or may not correspond to 
 * a single human.
 */
public interface User
extends UserGroupMember, TLEntity
{
    /**
     * <p>
     * Username of the user.  This may or may not be a string that the user
     * uses for authentication (depending on authentication method, for
     * example).
     * </p>
     * 
     * <p>Usernames are unique within timeline.</p>
     * 
     * @return user name, not <code>null</code>
     */
    @NotNull
    @NotEmpty
    public String username();
    
    /**
     * Display name of the user.  This is usually a user's "full" name,
     * however as the definition of "full" varies from user to user, country 
     * to country, etc. no attempt is made to partition the user's name into 
     * more than simply a single string.
     * 
     * @return display name of the user, not <code>null</code>, may be empty
     */
    @NotNull
    public String displayName();

    /**
     * Whether the user is active.  A user may be temporarily disabled in 
     * cases where their access should be disallowed for a period of time, but 
     * their account will need to be re-enabled later.
     * 
     * @return <code>true</code> if the user account is active, <code>false</code>
     * otherwise
     */
    public boolean active();

    /**
     * When the user was created.
     * 
     * @return creation time
     */
    @NotNull
    public Instant creationTime();

    /**
     * When the user was deleted (if the user was deleted).
     * 
     * @return an {@link Optional optional} containing the deletion time of 
     * the user (if deleted) or nothing (if user is still active)
     */
    public Optional<Instant> deletionTime();

}