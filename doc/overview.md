*Note: throughout this document, "TIMELINE" refers to the project itself, 
while "timeline" or other not-all-caps variants refer to the logical entity.*

# Overview

TIMELINE's logical model is based around the concept of a timeline, to which
entities may be added.  The addition of an entity to the timeline or the
modification of an entity on the timeline results in the occurrence of an
event.

TIMELINE is, therefore, event-driven.  Users may define (pre-)conditions that
apply to events, as well as actions that result from the occurrence of events.
In this sense, TIMELINE's logic is almost entirely controlled by the users of
the system.  TIMELINE itself provides only two main features: 1) a high-level
model allowing for the definition of project, entities, timelines, etc. 2) a
rules engine which may be used to place constraints on the modification/access
of entities and actions that occur in response to events.

## Domain Model

This section documents the "first class" model entities of TIMELINE, i.e those
which are defined as part of the system itself.  TIMELINE allows for a
user-defined data model; any entities created in this manner are naturally not
documented here, as they are considered user data.

### User

Users are one of the few "first class" model entities within TIMELINE.  A user
consists of a human-readable identifier, a display name, and the usual
assortment of uninteresting implementation detail attributes (such as creation
timestamp).

Usernames are unique and immutable within TIMELINE, however their format (and
even whether or not they are provided by end-users in the first place) may
depend on the authentication system that is ultimately used.  If a TIMELINE
instance is configured to use [micreda](https://gitlab.com/cscc/micreda), it
is likely that usernames will not be configured by users at all, and that the
username attribute of a user will simply contain whatever is received from the
token issued by micreda.

### User Group

A user group is a simple logical grouping of one or more users or user groups.

Groups may be nested to any depth.

Groups are intended for use by the access control systems of TIMELINE as well
as by various other features (e-mail recipient grouping, for example).

### Project

A project acts as a container for other entities, akin to a directory in a
filesystem.  Projects may be infinitely nested.

Projects serve three important roles.  First, they allow for the grouping of
related timelines/entities according to real-world organizational
requirements. Second, they are the level at which rules and application
preferences (such as internationalization strings) may be set.  And finally,
projects define the archetypes of the entities which may be created beneath
them.


### Timeline

A timeline in TIMELINE contains relationships to all "things that happen along
this timeline".  More formally, a timeline is a directed, acyclic graph, the
edges of which represent temporal relationships and the vertices of which
represent user-defined "entities".  All timelines have  a single root vertex
to which all "things along the timeline" have at least one edge.  
This root vertex is not displayed to the user -- it exists solely for use when
a reference to the entire graph is required.

A timeline exists within a project, and may only contain references to
entities also within that project.

#### "Width"

While one may think of a timeline as having a distinct start and end point,
the graph-based approach means that the timeline entity itself actually has no
finite duration.  Instead, it is effectively infinite, its "width" being an
inferred attribute determined by the (chronologically) first and last edges
rather than a persistent property.

### Entity

An entity is a user-defined, user-created object, the properties of which are
defined by its archetype.  It may be thought of as a simple mapping of
property names to values.

As entities exist "on" a timeline, there must exist at least one edge from the
timeline to the entity.

### Event

An event represents a change in the state of a timeline (inclusive of changes
to existing entities "along" the timeline).

More formally, an event is the addition, replacement, or removal of a vertex
within a timeline.

#### Event Phases

Events occur in two phases: pre-commit, and commit.

TIMELINE takes a similar approach to state changes as some "copy on write" or
"append-only" filesystems (e.g. ZFS) do.  When the state of a timeline is
changed, first the new state is created, then it is persisted (and becomes the
canonical representation of the state).  In other words: all timeline changes
occur in an atomic fashion.

One may therefore think of an event as the transition between these states,
and thus anything that happens in response to an event happening after  
state creation but before state persistence.

### Rule

Rules model TIMELINE's response to the generation of an event.  They may be
evaluated after either the pre-commit phase of an event or after the commit
phase.

A rule consists of one or more Conditions and one or more Actions.

#### Condition

A condition is an expression which, when supplied with two operands (an event,
and the timeline on which it occurs) evaluates to either true or false.  It
may be used to make certain Actions occur only in response to specific
modifications or to enforce various requirements for entity modification,
creation, etc.

An example of a constraint might test whether the value of a given property on
the entity being changed is greater than or equal to a known constant.

Conditions may be arbitrarily nested; to accomplish this, a special
CombinedCondition type allows for the combination of two or more conditions.
These conditions are evaluated in a given order and their results combined
with a specified boolean operator (`OR`, `AND`, `XOR`, `NOR`).

#### Action

An action represents a task performed by TIMELINE in response to an event. This
is relatively open-ended by nature: this flexiiblity allows for TIMELINE to be
usable for a variety of functions.

Actions are constrained by a few basic requirements in the interests of system
stability and security.  Runtime (wall-clock and CPU), thread creation,
network usage, etc. may all be restricted to ensure predictable performance.

Some examples of actions that will be pre-configured by TIMELINE include (but
are not limited to):

* Send an e-mail with a given body template to a set of users/groups
* Issue a GET/POST/PUT request containing the details of the event to a given
  endpoint
* Prevent the commit of the event (aka. rollback)
* Create an entity of a given type, populated with a given set of properties
  (possibly from the entity which generated the event) and associate it with a
  given timeline

While the user will be able to configure actions (i.e. set properties which
govern the behavior of an action that is part of a rule), this is more akin to
configuring *instances* of actions.  Since letting end users write arbitrary
code which will execute on a shared instance is either very difficult or
impossible to do while preserving security and stability (depending on whom
you ask) the actions themselves will be considered pre-defined parts of
TIMELINE, with any configurability being in the form of setting
parameters/properties/etc.

That said, to enable additional flexibility without recompilation of the main
application, it is expected that TIMELINE will use an SPI-type mechanism to
allow actions to be loaded from external libraries, possibly without instance
restart

### Rule Types

For ease of description, we model rules that occur during the pre-commit and
commit phases as distinct sub-types: constraints and reactions, accordingly.

#### Constraint

A Constraint is a rule which contains one or more conditions and an action
which prevents the "commit" phase from occurring (think: constraint causing
transaction rollback in an RDBMS).

Constraints are evaluated as part of the pre-commit phase of an event.

If the conditions are met, the event which triggered the rule evaluation will
be rejected, and the persistent state of the timeline will not change.

For ease of usage, a rule of this sort may be inferred by negating a set of
user-defined constraints, since negation is often cumbersome for people to
work with, but easy for software.  So, for example, rather than a user
specifying *rejection* of an event if a given property is over `15` or under
`10`, the user interface may allow the user to express that they want to
*allow* an event if the property is between `10` and `15` inclusive.

#### Reaction

A reaction is a rule which contains one or more conditions and one or more
actions.

Reactions are evaluated as part of the commit phase of an event.

Actions specified within a Reaction are best-effort.  Failures will be
reported, but will not result in any state changes.

##### Implementation note
 
We should *not* attempt to enforce any sort of ordering of the actions.  Since
these are almost always going to be inherently asynchronous things (like
issuing an HTTP request to something else) we want to be able to do these
whenever and however we choose, with the only guarantee provided to the user
being "it will be done at some point in the future."

## Append-only timelines

TODO - do we want to have timelines be "append-only"?

If so, what would be the best way to handle updates to entities?  Mark old
state as "deleted" and associate clone of entity?

This will carry some overhead, but it probably won't be too bad.  We should be
able to prune out deleted entities at query time and only load those on-demand
(when the front-end does some fancy Time Machine sorta thing or whatever).

The biggest cost will likely be storage, as this approach means that every
change will result in a new entity being created.  On the other hand, disk
space is exceedingly cheap.  Plus, there's no reason that we can't offer users
a way to "prune" previous items.  Assuming the rules engine works correctly,
we should be able to delete stale states of the timeline without causing any
logical inconsistencies.

All very hippy functional programming-ish.