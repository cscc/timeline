package edu.unc.cscc.timeline.model;

import java.util.Map;
import java.util.Optional;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * A project exists to organize entities, timelines, etc.  It is akin to a 
 * directory in a filesystem.
 */
public interface Project
extends TLEntity
{
    /**
     * Project name.  User-facing, serves as a user identifier of the project.
     * 
     * @return name of project, not <code>null</code>
     */
    @NotNull
    @NotEmpty
    public String name();

    /**
     * Human-friendly description of the project.
     * 
     * @return project description, not <code>null</code>, may be empty
     */
    @NotNull
    @NotEmpty
    public String description();

    /**
     * The project to which this project belongs (if any).  
     * 
     * @return optional referring to the parent project (if one exists), 
     * not <code>null</code>
     */
    @NotNull
    public Optional<Project> parent();

    /**
     * Project preferences.  Typically contains small flags governing 
     * user-facing behaviors (ex. date format).  Contains only preferences for
     * the project -- does not include those of its parent.
     * 
     * @return map of project preferences, not-<code>null</code>, may be empty.
     */
    @NotNull
    public Map<String, Object> preferences();

    /**
     * Get the project preference with the given name, attempting to cast to 
     * the given type.</p>
     * 
     * <p>
     * Implementations of this method will "cascade" up to the parent project
     * if no preference with the given identifier exists for the project on
     * which it is originally invoked.
     * </p>
     * 
     * @return preference value, or <code>null</code> if no such preference is
     * set
     */
    public <T> T getPreference(String preference, Class<T> type);

}