package edu.unc.cscc.timeline.model.user;

import edu.unc.cscc.timeline.model.TLEntity;

/**
 * <p> A member of a user group.  
 * </p>
 *
 * <p>This interface exists solely to allow group nesting -- it has no
 * properties of its own. </p>
 *
 */
public interface UserGroupMember
extends TLEntity
{
    
}