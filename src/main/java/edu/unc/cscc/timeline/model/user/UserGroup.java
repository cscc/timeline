package edu.unc.cscc.timeline.model.user;

import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import edu.unc.cscc.timeline.model.TLEntity;

/**
 * A user group.  Consists of 1+ groups/users.
 */
public interface UserGroup
extends TLEntity
{

    /**
     * Get the identifier of the group.  The identifier should be human-readable,
     * and unique within an instance of timeline.
     * 
     * @return identifier, not {@code null}, not empty
     */
    @NotNull
    @NotEmpty
    public String identifier();

    /**
     * Get the description of the group.  The description is a human-facing 
     * string describing the group's contents, function, etc.  It is optional.
     * 
     * @return description, not {@code null}, may be empty
     */
    @NotNull
    public String description();

    /**
     * Get the members of the group.
     * 
     * @return members, not {@code null}, not empty
     */
    @NotNull
    @NotEmpty
    public Set<UserGroupMember> members();
}