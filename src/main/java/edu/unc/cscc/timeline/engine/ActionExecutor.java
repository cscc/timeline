package edu.unc.cscc.timeline.engine;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import edu.unc.cscc.timeline.model.rules.Action;
import edu.unc.cscc.timeline.model.timeline.TEdge;

/**
 * <p>
 * Implements the logic of an {@link Action}.
 * </p>
 * 
 * <p>
 * Implementations are basically trusted code.  This means that you 
 * (the implementer) need to handle failure gracefully, ensure reasonable 
 * timeouts, etc.  Failure to do this can destabilize an instance.
 * </p>
 */
public interface ActionExecutor<T extends Action>
{
    /**
     * Get the identifier of the action which may be executed by this 
     * executor.
     * 
     * @return identifier, not {@code null}, not empty
     */
    @NotNull
    @NotEmpty
    public String identifier();

    /**
     * Get the action specification type which may be executed by this
     * executor.
     *
     * @return spec type, not {@code null}
     */
    @NotNull
    public Class<T> specClass();

    /**
     * Execute the specified action.
     * 
     * @param context context specifying the action to execute
     */
    public void execute(ExecutionContext<T> context);


    public interface ExecutionContext<A extends Action>
    {
        /**
         * The time in which the action is allowed to execute (i.e. when the
         * engine will kill the executing thread.)  Executors should be able to 
         * complete (or fail to complete) their execution within this time period;
         * if an executor knows that it cannot complete execution within this 
         * time frame, it should {@link ExecutionContext#reject() reject} the
         * execution.
         *
         * @return allowed execution time (in milliseconds), will be
         * non-negative
         */
        @Min(1)
        public int timeout();

        /**
         * Called by the action executor when the action has been successfully
         * completed.  This method may only be invoked once, as the thread in
         * which the execution takes place is liable to be killed upon 
         * invocation.
         */
        public void complete();

        /**
         * Reject execution of the action.  An action executor may call this 
         * method when it is unable to execute the specified action in the 
         * alotted time.
         */
        /* IMPLEMENTATION + DESIGN NOTE: we handle rejection seperately so
         * that we can later support cool things, like re-attempting actions
         * with a larger timeout for non-time-sensitive rulesets, better
         * handling of reaction to events of a non-interactive origin (where
         * timeouts can be relaxed), etc.
         *
         * Of course we can't do that later if we don't let the executor
         * implementers distinguish between failures for external reasons and
         * failures due to insufficient time budgets.
         */
        public void reject();

        /**
         * Convenience method for
         * {@link ExecutionContext@fail(String, Throwable)} which does not
         * specify a cause.
         *
         * @param message message describing the failure, not {@code null},
         * not empty
         */
        public default void fail(@NotNull String message)
        {
            this.fail(message, null);
        }

        /**
         * Called by the executor to indicate a failed execution.
         *
         * @param message message describing the failure, not {@code null},
         * not empty
         * @param cause underlying cause of the failure (if known), or
         * {@code null} if the cause is not available
         */
        public void fail(String message, Throwable cause);

        /**
         * The action to execute.
         * 
         * @return action, not {@code null}
         */
        @NotNull
        public A action();

        /**
         * The event (edge) which the action is executed in response to.
         * 
         * @return event (edge), not {@code null}
         */
        @NotNull
        public TEdge edge();

    }
}