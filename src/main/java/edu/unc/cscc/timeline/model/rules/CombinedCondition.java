package edu.unc.cscc.timeline.model.rules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

public final class CombinedCondition
implements Condition
{
    public static final String      IDENTIFIER = "combine";

    private final UUID              id;
    private final List<Condition>   conditions;
    private final Operator          operator;

    @JsonCreator
    public CombinedCondition(@JsonProperty("id") UUID id, 
                            @JsonProperty("operator") Operator operator, 
                            @JsonProperty("conditions") 
                                List<Condition> conditions)
    {
        this.id = id;
        this.operator = operator;
        this.conditions = new ArrayList<>(conditions);
    }

    @Override
    public final String identifier()
    {
        return IDENTIFIER;
    }

    @Override
    public UUID id()
    {
        return this.id;
    }

    @JsonGetter("conditions")
    public final List<Condition>
    conditions()
    {
        return Collections.unmodifiableList(this.conditions);
    }

    @JsonGetter("operator")
    public final Operator
    operator()
    {
        return this.operator;
    }
    
    public static enum Operator
    {
        OR("or", "any", "any-of"),
        XOR("xor", "one", "only-one-of"),
        NOR("nor", "none", "none-of"),
        AND("and", "all", "all-of");

        private final String[]      tokens;

        private Operator(String ... tokens)
        {
            this.tokens = tokens;
        }

        public final String[] tokens()
        {
            return this.tokens;
        }

        @Override
        @JsonValue
        public final String
        toString()
        {
            return this.tokens[0];
        }

        @JsonCreator
        public static final Operator
        fromToken(String token)
        {
            for (final Operator o : Operator.values())
            {
                for (final String s : o.tokens)
                {
                    if (s.equals(token))
                    {
                        return o;
                    }
                }
            }

            return null;
        }

    }
}