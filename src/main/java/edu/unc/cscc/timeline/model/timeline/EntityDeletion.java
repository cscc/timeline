package edu.unc.cscc.timeline.model.timeline;

/**
 * Edge representing deletion of an entity.  Originating vertex is the entity
 * to be deleted, terminating is an instance of {@link TerminalVertex}.
 */
public interface EntityDeletion
extends TEdge
{

    /* TODO implement*/


    /**
     * Terminal node; for use as the destination of edges representing deletion
     * of an entity.
     */
    public static final class TerminalVertex
    implements TVertex
    {
        private static final TerminalVertex INSTANCE = new TerminalVertex();

        /**
         * Get a singleton instance of {@link TerminalVertex}.
         * 
         * @return singleton
         */
        public static final TerminalVertex
        instance()
        {
            return INSTANCE;
        }
    }

}