package edu.unc.cscc.timeline.model.rules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.timeline.model.timeline.TEdge.EventType;

/**
 * A reaction is a rule which applies after an event has entered its commit
 * phase.  It contains 1+ conditions and 1+ actions; a user-defined ordering
 * is defined for the conditions, but not for the actions.
 */
@JsonTypeName("reaction")
public final class Reaction
implements Rule
{
    private final Set<Action>                   actions;
    private final EventType                     eventType;
    private final String                        decription;
    private final List<Condition>               conditions;
    private final UUID                          id;

    public Reaction(UUID id, String description,
                        List<Condition> conditions, Set<Action> actions,
                        EventType appliesTo)
    {
        this.id = id;
        this.decription = description;
        this.conditions = new ArrayList<Condition>(conditions);
        this.actions = new HashSet<Action>(actions);
        this.eventType = appliesTo;
    }

	@Override
    public UUID id()
    {
		return this.id;
	}

    @Override
    public String
    description()
    {
        return this.decription;
    }

	@Override
    public @NotNull @NotEmpty List<Condition> 
    conditions() 
    {
		return Collections.unmodifiableList(this.conditions);
	}

	@Override
    public @NotNull @NotEmpty Set<Action> 
    actions() 
    {
		return Collections.unmodifiableSet(this.actions);
	}

	@Override
    public boolean preCommit() 
    {
		return false;
    }
    
    @Override
    public EventType
    appliesTo()
    {
        return this.eventType;
    }

}