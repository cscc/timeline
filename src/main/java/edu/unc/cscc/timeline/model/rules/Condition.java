package edu.unc.cscc.timeline.model.rules;

import com.fasterxml.jackson.annotation.JsonGetter;

import edu.unc.cscc.timeline.model.TLEntity;
import edu.unc.cscc.timeline.model.timeline.TEdge;

/**
 * A condition imposed upon the vertex pair specified by an 
 * {@link TEdge edge} representing an event.
 * 
 * <p>
 * Note that this object serves merely as a specification of a condition; the 
 * implementation of the condition's logic will be done elsewhere, specifically
 * in an instance of {@link edu.unc.cscc.timeline.engine.ConditionEvaluator}
 * </p>
 */
public interface Condition
extends TLEntity
{

    @JsonGetter("identifier")
    public String identifier();

}