package edu.unc.cscc.timeline.model.timeline;

/**
 * Edge representing modification of an entity.  Originating vertex is original
 * version of entity, terminating vertex a copy reflecting the modification.
 */
public interface EntityModification
extends TEdge
{
        /* TODO implement */

}