Unanswered design questions
===

This document is used for brainstorming around unanswered design questions.

Persistence of all changes to rulesets
---

We could conceivably handle rules the same way that we handle the actual graph
proper: no deletions/modifications, just additions of new entities which may
or may not represent modifications or deletions.

The upshot of this is that we could (provided access to executors for the
actions/conditions) replay an entire sequence of events *and the resulting
actions* for any time period that we please.  That's cool.

The downside of this is that it's a lot more stuff to store, plus we don't get
any guarantees that we actually can do this -- we can certainly do it for
actions that are part of the application, but what about ones that are implemented
external to the application and loaded via our SPI-ish thing?  

In other words: if the whole point is deterministic behavior even when
re-playing the past, doesn't the lack of history for the implementations sort
of defeat that?  Furthermore, what if we don't want some of the actions
replayed (such as ones that talk to production systems, or send emails?) Well
we could implement a filtering system for the replay, but that's a *lot* of
additional complexity for something that we probably don't need per se.

Now that I think about it, a simple API change would make this easier
(assuming that one can trust Action authors to adhere to the documented API):
let actions declare if they change state external to the system.  By necessity
actions which declare such a change will be excluded from replay, and will
lose the ability to change the application's state.  This will allow for 
complete replay of all state changes without the risk of impacting external 
state.

If this were Kickstarter, let's consider this to be the $5MM stretch goal.

Handling of events in parallel
---

Provided that we don't allow actions to modify state (see below), we can
easily execute the rules for a given event in parallel (plus the actions for a
given reaction).  

But what about handling multiple events in parallel?

Well if we don't allow state modification by events, then the answer is yes, 
provided that we don't guarantee consistency *during* execution, but only 
eventual consistency after execution of all queued events.

We may just start off doing this, and simply convert to sequential
implementation if we need to support actions modifying state later on.

Actions which modify state are going to require some fancy footwork.  We can
(assuming Action authors adhere to our "reproducible, side-effect-free" rule)
reason about the sequencing of Actions and the subsequent, additional events.
As a result, we should be able to determine whether or not a given event
results in a satisfiable set of Reactions/Events.

The easiest approach is to create a context of some sort for handling an Event
stemming from a non-Action source, and start tracking the IDs of the Actions
that execute.  It's not really state per se, but it is state that's tracked
outside of the Reactions themselves.  Track the IDs with a ref count, and when
you start seeing duplicates you are *likely* in a cycle.  You can't prove it,
since Actions can do anythin that Java allows (ex: add a new entity the first
500 invocations, simply return on call 501)... but at that point you just have
to trust that the Action implementers read some of the docs and know what
they're doing.

We only really care about catching mistakes, not intentional stupidity.

---

There are several design challenges to implementing actions which modify 
state.

* These changes will not be visible to other actions executing in response to
  the same event unless we are willing to let the user impose an order on
  actions. And if we do that, we're seriously hampering our ability to
  parallelize execution.
* In order to keep our state graph valid, actions may not modify/delete the
  entity that they are executed in response to.  It would be impossible to
  guarantee consistency if actions can axe their triggers.
* This will make it rather miserable to solve if we also allow conditions to
  see the entire graph.  The only way to handle this will be to re-evaluate
  conditions after every evaluation, which (in addition to the overhead) will
  make implementation more complex, and substantially harder to reason about.
  Still doable, but potentially not worth it.  Of course, we may just pass on
  letting conditions see the entire graph and side-step this part of the
  problem.

External condition evaluator implementations
---

Far from the best idea.  Conditions should be able to execute relatively 
quickly, and although we can impose timeouts on them, that's a lot of work
and housekeeping.

More importantly is the issue of determinism.  The conditions that we're
planning on implementing are -- barring particularly terrible bugs --
deterministic.  Third party implementations cannot have the same guarantee
made for them, so this would open a door to a potentially non-deterministic
rules engine instance... which is pretty much the exact opposite of what we'd
want in a rules engine.

Of course organizations which really need more complex conditions are free to
fork and implement their own... but we should probably not make it easy by
building an extension mechanism, etc. for this.  If you're forking the application,
you probably have developers who understand the importance of deterministic
behavior here.

Middleware?
---

It may not be a bad idea to encapsulate some of the business-specific logic 
into a [pluggable?] layer that sits under the controller layer, but above the 
rules engine proper.  

The idea is this:

1) The UI expects its calls to the back-end to be handle atomically, i.e. some
   UI action either results in a successful server-side set of actions, or an
   unsuccessful set.
2) Rather than make the rules engine too complex for non-developers to use, we
   can extract some of the complexity to very-nearby client code.  It still
   uses the rules engine, and may even handle setup for a given project, but
   it also is capable of translating a single call to multiple changes to the
   underlying state (example: create a user, create an associated entity,
   modify an existing entity, all in addition to a "create new user" method 
   invocation).
3) By simplifying the rules engine, we it easier to be provably correct.  For
   something whose selling point is a guarantee of consistency of state across
   time, this is important.

This does result in an additional layer, but on the plus side that also allows
for easier scaling.  The controller, business-logic, and actual execution
layers are now all easily separated however one wishes.

It remains to be seen if the pitfalls of this design (namely the temptation to
have the "business logic" layer just do all the work that the rules engine
should) will outweigh the benefits.

This requires some further experimentation.