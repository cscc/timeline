package edu.unc.cscc.timeline.model.timeline;

import com.google.common.graph.Network;

public interface TGraph
extends Network<TVertex, TEdge>
{

    public Timeline timeline();
}