package edu.unc.cscc.timeline.model.timeline;

import java.time.Instant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonValue;

import edu.unc.cscc.timeline.model.user.User;

/**
 * Represents an edge (association) within a timeline graph.  An edge represents
 * an implict change of state, either in the form of a revision of an existing
 * entity, the addition of a new entity to the timeline, or the deletion of an
 * entity.
 */
public interface TEdge
{
    /**
     * When the change was made.
     * 
     * @return time when the change was made, not <code>null</code>
     */
    @NotNull
    public Instant time();

    /**
     * The user responsible for the change.
     * 
     * @return user responsible for the change, not <code>null</code>
     */
    @NotNull
    public User user();

    /**
     * Get the event type that this edge represents.  Although this can be 
     * determined from the sub-type of the edge, this property allows for 
     * edge type to be determined without knowledge of the implementation's
     * class structure.
     * 
     * @return event type, not {@code null}
     */
    @NotNull
    public EventType type();

    public static enum EventType
    {
        ADDITION("addition"),
        MODIFICATION("modification"),
        DELETION("deletion");

        private final String      str;

        EventType(String str)
        {
            this.str = str;
        }

        /**
         * <p>
         * Get the token that represents the event type, suitable for 
         * serialization (such as for rule definitions in external files).
         * </p>
         * 
         * <p>
         * This identifier should be kept stable, as external rules may 
         * depend on it.  It also must be suitable for use within, JSON,
         * YAML, and XML -- and thus should probably be limited to ASCII, 
         * ideally alphanumeric characters.
         * </p>
         * 
         * @return token representing the event type, not {@code null}
         */
        @NotNull
        @NotEmpty
        @JsonValue
        public String
        token()
        {
            return this.str;
        }

        /**
         * Get the event type which corresponds to the given token.
         * 
         * @return event type, or {@code null} if no event type with the given
         * token exists
         */
        public static EventType
        forToken(String token)
        {
            for (EventType type : EventType.values())
            {
                if (type.token().equals(token))
                {
                    return type;
                }
            }
            return null;
        }
    }
}