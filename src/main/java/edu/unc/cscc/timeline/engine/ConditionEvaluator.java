package edu.unc.cscc.timeline.engine;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import edu.unc.cscc.timeline.model.rules.Condition;
import edu.unc.cscc.timeline.model.timeline.TEdge;

public interface ConditionEvaluator<T extends Condition>
{
    /**
     * Evaluate the given condition against the given edge.
     * 
     * @param condition condition to evaluate, not {@code null}
     * @param edge edge against which to evaluate, not {@code null}
     * @return evaluation result
     */
    public boolean evaluate(@NotNull T condition, 
                            @NotNull TEdge edge);

    /**
     * Get the specification class against which definitions of this condition
     * may be mapped.
     * 
     * @return specification class
     */
    public Class<? extends T> specClass();

    /**
     * The identifier of the condition that the evaluator can evaluate.
     * 
     * @return identifier, not {@code null}, not empty
     */
    @NotNull
    @NotEmpty
    public String identifier();                                                                    
}