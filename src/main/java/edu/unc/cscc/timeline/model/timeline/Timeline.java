package edu.unc.cscc.timeline.model.timeline;

import edu.unc.cscc.timeline.model.Project;

/**
 * A timeline.  Contains little to no information of its own, as its function
 * is mostly as a vertex to which edges point.
 */
public interface Timeline
extends TVertex
{
    public Project project();
}