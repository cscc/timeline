package edu.unc.cscc.timeline.engine;

import java.util.Collection;

import edu.unc.cscc.timeline.model.rules.Constraint;
import edu.unc.cscc.timeline.model.rules.Reaction;
import edu.unc.cscc.timeline.model.timeline.TEdge.EventType;

public final class Ruleset
{

    public Collection<Reaction> reactions()
    {
        return null;
    }

    public Collection<Reaction> reactionsTo(EventType eventType)
    {
        return null;
    }


    public Collection<Constraint> constraints()
    {
        return null;
    }

    public Collection<Constraint> constraintsFor(EventType eventType)
    {
        return null;
    }
}