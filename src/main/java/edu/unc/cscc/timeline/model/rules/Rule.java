package edu.unc.cscc.timeline.model.rules;

import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;

import edu.unc.cscc.timeline.model.TLEntity;
import edu.unc.cscc.timeline.model.timeline.TEdge.EventType;

/**
 * <p>
 * A rule is a set of 1+ conditions which will be evaluated in response to an 
 * event.  If all conditions are met, the set of 1+ actions specified by the
 * rule will be performed.  Rules may be evaluated during either phase of 
 * event handling (depending on {@link Rule#preCommit() how they are defined.})
 * </p>
 * 
 * <p>
 * This interface serves as a common ancestor for {@link Constraint constraints}
 * and {@link Reaction reactions}, but is probably not particularly useful for
 * direct implementation.
 * </p>
 */
public interface Rule
extends TLEntity
{
    /**
     * An (optional) human-readable description of the rule.
     * 
     * @return description, may be {@code null}
     */
    @JsonGetter("description")
    public String description();

    /**
     * The conditions which must be met for this rule's actions to be executed.
     * 
     * @return conditions, not {@code null}, not empty
     */
    @NotNull
    @NotEmpty
    @JsonGetter("conditions")
    public List<Condition> conditions();

    /**
     * The actions which this rule defines for execution after the conditions
     * have been met.
     * 
     * @return actions, not {@code null}, not empty
     */
    @NotNull
    @NotEmpty
    @JsonGetter("actions")
    public Set<Action> actions();

    /**
     * Whether the rule should be evaluated during the pre-commit phase.
     * 
     * @return {@code true} if the rule should be evaluated in the pre-commit
     * phase, {@code false} if the rule should be evaluated after the commit
     * phase
     */
    public boolean preCommit();

    /**
     * The event type to which the rule is applied.
     * 
     * @return event type, not {@code null}
     */
    @NotNull
    public EventType appliesTo();
    
}