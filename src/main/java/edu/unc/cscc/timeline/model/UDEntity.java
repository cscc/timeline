package edu.unc.cscc.timeline.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * A user-defined entity.  A user-defined entity will serve as the archetype
 * for instances containing user data.
 */

/* Note to the future maintainer:

    Yes, I know about the "inner-platform-effect" (as written about in  The
    Daily WTF).  We are indeed walking quite a fine line here -- future
    maintainers (aka YOU) will need to tread carefully to avoid becoming an
    example of this.
*/
public interface UDEntity
extends TLEntity
{

    /**
     * Get all fields defined by the entity.
     *
     * @return entity's fields, may be empty
     */
    @NotNull
    public Set<UDField> fields();

    /**
     * Canonical name of the entity.  Does not necessarily follow Java,
     * Javascript, or any other naming conventions (since users are not
     * necessarily developers, and vice versa.)
     * 
     * @return entity name, not <code>null</code>, not empty, not whitespace
     */
    @NotNull
    @NotEmpty
    @Pattern(regexp = "^(?=.*\\S).+$")
    public String name();

    /**
     * A description of the entity.  User-facing, holds no meaning to the 
     * application itself.
     * 
     * @return description of the entity, not <code>null</code>, may be empty
     */
    @NotNull
    public String description();

    /**
     * The project with which the entity type is associated.  If no project 
     * is set, the entity is available globally.
     * 
     * @return project, or empty optional if global
     */
    @NotNull
    public Optional<Project> project();

    /**
     * A user-defined field of an entity (aka an object property.)
     */
    public static final class UDField
    {
        private final FieldType     type;
        private final String        name;
        private final String        description;

        public UDField(String name, FieldType type)
        {
            this(name, type, null);
        }

        public UDField(String name, FieldType type, String description)
        {
            this.name = name;
            this.type = type;
            this.description = description;
        }

        /**
         * Get the name of the field.
         * 
         * @return name, not {@code null}
         */
        @NotEmpty
        public final String
        name()
        {
            return this.name;
        }

        /**
         * Get the type of the field.
         * 
         * @return type, not {@code null}
         */
        @NotNull
        public final FieldType
        type()
        {
            return this.type;
        }

        /**
         * Get the human-readable description of the field (if any.)
         * 
         * @return description, may be {@code null}
         */
        public final String
        description()
        {
            return this.description;
        }

        /**
         * Validate the field identifier to ensure that it conforms to the timeline
         * identifier naming rules.
         *
         * @return <code>true</code> if the identifier is valid,
         * <code>false</code> otherwise
         */
        public static boolean validFieldIdentifier(String identifier)
        {
            return StringUtils.isEmpty(identifier)
                    || StringUtils.containsWhitespace(identifier);
        }
    }
    
    public static enum FieldType
    {
        INTEGER,
        DECIMAL,
        BOOLEAN,
        STRING;

        /**
         * Get the Java type of the field type (i.e. the Java type best able 
         * to represent data collected against a field of this type.)
         * 
         * @return Java type
         */
        public Class<?> javaType()
        {
            switch (this)
            {
                case INTEGER:
                    return BigInteger.class;
                case DECIMAL:
                    return BigDecimal.class;
                case BOOLEAN:
                    return Boolean.class;
                case STRING:
                default:
                    return String.class;
            }
        }
    }
}