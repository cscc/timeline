package edu.unc.cscc.timeline.model.rules;

import javax.validation.constraints.NotNull;

import edu.unc.cscc.timeline.model.TLEntity;

/**
 * <p>
 * An action that may be performed in response to an event.  Actions may 
 * happen in either phase of the event lifecycle (depending on how they are
 * specified.)
 * </p>
 * 
 * <p>
 * Note that this object serves merely as a specification of an action; the 
 * implementation of the action's logic will be done elsewhere, specifically
 * in an instance of {@link edu.unc.cscc.timeline.engine.ActionExecutor}
 * </p>
 * 
 */
public interface Action
extends TLEntity
{
    /**
     * Identifier for the action.  May not be human-friendly.
     * 
     * @return identifier, not {@code null}
     */
    @NotNull
    public String identifier();
}
