package edu.unc.cscc.timeline.engine;

/**
 * Indicates a failure during an action's execution.
 */
public class ExecutionException
extends Exception
{

    private static final long serialVersionUID = 1L;
    
    public ExecutionException(String message)
    {
        super(message);
    }

    public ExecutionException(String message, Throwable cause)
    {
        super(message, cause);
    }
}