package edu.unc.cscc.timeline.model.timeline;

import java.util.Map;

import javax.validation.constraints.NotNull;

import edu.unc.cscc.timeline.model.UDEntity;

public interface EntityInstance
extends TVertex
{
    /**
     * The archetype of the entity (i.e. the entity definition specifying 
     * the fields, name, etc. of the entity).  Akin to {@link Object#getClass()}.
     * 
     * @return field archetype, not <code>null</code>
     */
    @NotNull
    public UDEntity archetype();

    /**
     * Get the values for the entity's fields.
     * 
     * @return map of field identifiers to values, not <code>null</code>
     */
    @NotNull
    public Map<String, Object> fields();

    /**
     * Get the field with the given identifier, casting the value to the 
     * given type. 
     * 
     * @return value of the field, or <code>null</code> if the field does
     * not exist (or the value is not of the given type)
     */
    public <T> T field(String identifier, Class<T> cls);
}