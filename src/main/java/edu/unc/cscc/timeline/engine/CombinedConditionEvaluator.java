package edu.unc.cscc.timeline.engine;

import java.util.function.Function;
import java.util.stream.Stream;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.tuple.Pair;

import edu.unc.cscc.timeline.model.rules.CombinedCondition;
import edu.unc.cscc.timeline.model.rules.CombinedCondition.Operator;
import edu.unc.cscc.timeline.model.rules.Condition;
import edu.unc.cscc.timeline.model.timeline.TEdge;

/**
 * Special-case evaluator for combined conditions.
 */
public final class CombinedConditionEvaluator
implements ConditionEvaluator<CombinedCondition>
{
    private Function<Condition, ConditionEvaluator<?>>      lookupFn;

    public CombinedConditionEvaluator(
                Function<Condition, ConditionEvaluator<?>> lookupFn)
    {
        this.lookupFn = lookupFn;
    }

	@Override
    public Class<? extends CombinedCondition> 
    specClass()
    {
		return CombinedCondition.class;
	}

	@Override
    public @NotNull @NotEmpty String 
    identifier()
    {
		return CombinedCondition.IDENTIFIER;
	}

    @Override
    @SuppressWarnings("unchecked")
    public boolean
    evaluate(@NotNull CombinedCondition condition, @NotNull TEdge edge)
    {
        Operator operator = condition.operator();

        /* check that we can get evalutors for all of the child conditions */
        String missingID = 
            condition.conditions()
                        .stream()
                        .filter(c -> null == lookupFn.apply(c))
                        .findFirst()
                        .map(c -> c.identifier())
                        .orElse(null);

        if (missingID != null)
        {
            throw new RuntimeException(String.format(
                    "Failed to find evaluator for condition with ID '%s'. " +
                    "This should have been caught by the engine.",
                    missingID));
                    
        }

        Stream<Pair<Condition, ConditionEvaluator<?>>> stream = 
            condition.conditions()
                        .stream()
                        .map(c -> Pair.of(c, lookupFn.apply(c)));

        switch (operator)
        {
            case OR:
                return stream.anyMatch(p -> 
                        ((ConditionEvaluator<Condition>) p.getRight())
                                    .evaluate(p.getLeft(), edge));
            case XOR:
                return stream.reduce(0, 
                            (i, pair) -> 
                                ((ConditionEvaluator<Condition>) pair.getRight())
                                    .evaluate(pair.getLeft(), edge)
                                ? 1 : 0,
                            (a, b) -> a + b) == 1;
            case NOR:
                return stream.noneMatch(p -> 
                        ((ConditionEvaluator<Condition>) p.getRight())
                                    .evaluate(p.getLeft(), edge));
            case AND:
            default:
                return stream.allMatch(p -> 
                        ((ConditionEvaluator<Condition>) p.getRight())
                                    .evaluate(p.getLeft(), edge));
        }
	}
}