package edu.unc.cscc.timeline.model;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * Base interface; mainly for serialization of all timeline entities.
 */
public interface TLEntity
{

    @JsonGetter("id")
    public UUID id();
}