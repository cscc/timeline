package edu.unc.cscc.timeline.model.rules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.timeline.model.timeline.TEdge.EventType;

/**
 * A constraint is a rule applied in the pre-commit phase, the evaluation of 
 * which may prevent the commit of the event for which it is evaluated.
 */
@JsonTypeName("constraint")
public final class Constraint
implements Rule
{

    private final UUID              id;
    private final String            description;
    private final List<Condition>   conditions;
    private final EventType         eventType;

    @JsonCreator
    public Constraint(@JsonProperty("id") UUID id, 
                        @JsonProperty("description") String description, 
                        @JsonProperty("conditions") List<Condition> conditions,
                        @JsonProperty("applies-to") EventType appliesTo)
    {
        this.id = id;
        this.description = description;
        this.conditions = new ArrayList<>(conditions);
        this.eventType = appliesTo;
    }

    @Override
    public Set<Action> 
    actions()
    {
        return Collections.singleton(RollbackAction.instance());
    }

    @Override
    public UUID 
    id()
    {
		return this.id;
    }
    
    @Override
    public String
    description()
    {
        return this.description;
    }

	@Override
    public @NotNull @NotEmpty List<Condition> 
    conditions() 
    {
		return Collections.unmodifiableList(this.conditions);
	}

	@Override
    public boolean 
    preCommit() 
    {
		return true;
    }
    
    @Override
    public EventType
    appliesTo()
    {
        return this.eventType;
    }

    /**
     * Pre-defined action indicating the rollback of an event.  Provides
     * a singleton instance.
     */
    public static final class RollbackAction
    implements Action
    {

        public static final String          IDENTIFIER = "ROLLBACK";

        private static final RollbackAction INSTANCE = new RollbackAction();

		@Override
        public UUID 
        id()
        {
			return null;
		}

		@Override
        public @NotNull String 
        identifier()
        {
			return IDENTIFIER;
        }
        
        public static final RollbackAction
        instance()
        {
            return INSTANCE;
        }

    }

	
}