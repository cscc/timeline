package edu.unc.cscc.timeline.engine;

import java.time.Instant;
import java.util.Iterator;

import edu.unc.cscc.timeline.model.timeline.TEdge;

/**
 * An iterable source of events.  The iterators produced by this source 
 * (either explicitly via {@link EventSource#iterator(Instant)} or implicitly
 * via {@link Iterable}'s usage of iterators) will NOT support modification,
 * but may handle changes to the underlying source of events (i.e. the 
 * addition of events after an iterator has been obtained.)  In other words, 
 * the source of events over which the iterators iterate is not guaranteed to 
 * be stable or finite.
 */
public class EventSource
implements Iterable<TEdge>
{

	@Override
    public Iterator<TEdge>
    iterator()
    {
		return this.iterator(Instant.MIN);
    }
    
    /**
     * Get an iterator over the events represented by this source which occur
     * on or after the given time.  Events will be iterated over in order of
     * occurrence.
     * 
     * @param time instant on or after which events will be returned
     * @return iterator over events represented by this source
     */
    public Iterator<TEdge>
    iterator(Instant time)
    {
        return null;
    }

}